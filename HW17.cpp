#include <iostream>

using namespace std;

class MyData
{
private:
    int a;
public:
    int GetA()
    {
        return a;
    }

    void SetA(int NewA)
    {
        a = NewA;
    }
};

class Vector
{
private:
    double x, y, z;
public:
    Vector(): x(0),y(0),z(0)
    {}
    Vector(double _x, double _y, double _z): x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        cout<<'\n' << x << " " << y << " " << z << '\n';
    }
    void VectorLen()
    {
        cout << "Vector length: " << sqrt(x * x + y * y + z * z);
    }
      
};

int main()
{
    // �������� ������ 1
    MyData test, test1;
    test.SetA(5);
    test1.SetA(10);
    cout << "Part 1" << '\n' << test.GetA() << " " << test1.GetA() << '\n';

    // �������� ������ 2
    Vector v;
    Vector v1(10,10,10);
    cout << "Part 2";
    v.Show();
    v.VectorLen();

    v1.Show(); 
    v1.VectorLen();

}
